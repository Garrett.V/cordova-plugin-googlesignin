package garrettv.cordova;
/**
 * Garrett Vernon
 */

import android.content.Context;
import android.content.Intent;
import com.android.volley.*;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class GoogleSignInPlugin extends CordovaPlugin {

    private CallbackContext callbackContext;
    private GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 9001;
    private String serverClientId;
    private final String suffix = ".apps.googleusercontent.com";
    private String serverUrl;

    private boolean isInitialised;

    @Override
    protected void pluginInitialize() {
        super.pluginInitialize();
        isInitialised = false;
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        this.callbackContext = callbackContext;
        Context context = cordova.getActivity().getApplicationContext();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(context);

        if(isInitialised && !action.equals("init")) {
            boolean isAction;
            switch (action) {
                case "signIn":
                    signIn(account);
                    isAction = true;
                    break;
                case "signOut":
                    signOut();
                    isAction = true;
                    break;
                case "revoke":
                    revokeAccess();
                    isAction = true;
                    break;
                case "getAccount":
                    if (account != null) {
                        callbackContext.success(getAccount(account));
                    } else {
                        callbackContext.error("Account is null");
                    }
                    isAction = true;
                    break;
                default:
                    callbackContext.error("There is no " + action + " defined");
                    isAction = false;
            }
            return isAction;
        }

        try {
            JSONObject options = args.getJSONObject(0);
            serverClientId = options.getString("serverClientId");
            serverUrl = options.getString("serverUrl");

            if (!validateClientId(serverClientId, suffix)) {
                callbackContext.error("Invalid server client ID, must end with " + suffix);
                return false;
            }
        } catch (JSONException e) {
            callbackContext.error("Arguments Error. Did you initialise with the serverClientId and the ServerURL?");
            return false;
        }
        init();
        return true;
    }

    private void init() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(serverClientId)
                .requestEmail()
                .build();
        // Build a GoogleSignInClient with the options specified by gso
        mGoogleSignInClient = GoogleSignIn.getClient(cordova.getActivity(), gso);
        isInitialised = true;
        callbackContext.success();
    }

    private JSONObject getAccount(GoogleSignInAccount account) {
        JSONObject accountJSON = new JSONObject();
        try {
            assert account != null;
            accountJSON.put("displayName", account.getDisplayName());
            accountJSON.put("givenName", account.getGivenName());
            accountJSON.put("familyName", account.getFamilyName());
            accountJSON.put("email", account.getEmail());
            accountJSON.put("photoUrl", account.getPhotoUrl());
            accountJSON.put("googleId", account.getId());
            return accountJSON;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void signIn(GoogleSignInAccount account) {
        if (account != null) {
            callbackContext.success(getAccount(account));
        } else {
            SignIntoGoogle();
        }
    }

    private void signOut() {
        mGoogleSignInClient.signOut().addOnCompleteListener(cordova.getActivity(), new OnCompleteListener<Void>() {
            @Override
            public void onComplete(Task<Void> task) {
                callbackContext.success("Signed out");
            }
        });
    }

    private void revokeAccess() {
        mGoogleSignInClient.revokeAccess()
                .addOnCompleteListener(cordova.getActivity(), new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(Task<Void> task) {
                        callbackContext.success("Access Revoked");
                    }
                });
    }

    private void SignIntoGoogle() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        cordova.setActivityResultCallback(this);
        cordova.getActivity().startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(intent);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (account != null && account.getIdToken() != null) {
                httpPOSTToServer(account.getIdToken());
            } else {
                callbackContext.error("No IdToken received");
            }
        } catch (ApiException e) {
            //TODO format this better
            callbackContext.error("Ah Man! - helpful error code: " + e.getStatusCode()
                    + " see https://developers.google.com/android/reference/com/google/android/gms/common/api/CommonStatusCodes");
        }
    }

    private void httpPOSTToServer(String idToken) {
        RequestQueue queue = Volley.newRequestQueue(cordova.getContext());
        /*Post data*/
        Map<String, String> jsonParams = new HashMap<>();
        jsonParams.put("idToken", idToken);

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, serverUrl,

                new JSONObject(jsonParams),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        handlePOSTResult(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callbackContext.error("That didn't work " + error.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        queue.add(postRequest);
    }

    private void handlePOSTResult(JSONObject response) {
        if (response.has("Success")) {
            callbackContext.success();
        } else {
            try {
                callbackContext.error("StatusCode: " + response.getInt("StatusCode") + " " + response.getString("ErrorMessage"));
            } catch (JSONException e) {
                callbackContext.error("Malformed JSON response " + e.getMessage());
            }
        }
    }

    private boolean validateClientId(String clientId, String suffix) {
        return clientId.trim().endsWith(suffix);
    }
}