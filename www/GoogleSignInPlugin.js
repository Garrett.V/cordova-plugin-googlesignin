function GoogleSignInPlugin() {
}

GoogleSignInPlugin.prototype.init = function (serverClientId, serverUrl, successCallback, errorCallback) {
    var options = {};
    options.serverClientId = serverClientId;
    options.serverUrl = serverUrl;
    cordova.exec(successCallback, errorCallback, "GoogleSignInPlugin", "init", [options]);
}

GoogleSignInPlugin.prototype.signIn = function (successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "GoogleSignInPlugin", "signIn");
}

GoogleSignInPlugin.prototype.signOut = function (successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "GoogleSignInPlugin", "signOut");
}

GoogleSignInPlugin.prototype.revoke = function (successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "GoogleSignInPlugin", "revoke");
}

GoogleSignInPlugin.prototype.getAccount = function (successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "GoogleSignInPlugin", "getAccount");
}

GoogleSignInPlugin.install = function (){
    if (!window.plugins) {
        window.plugins = {};
    }
    window.plugins.googlesignin = new GoogleSignInPlugin();
    return window.plugins.googlesignin;
};
cordova.addConstructor(GoogleSignInPlugin.install)